﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsCore.CoreModels
{
    public class AccessCoreModel
    {
        Guid id;
        Guid idProject;
        Guid idUser;
        AccessLevelCoreModel level;
        string login;

        public string Login
        {
            get { return login; }
            set { login = value; }
        }
        
        public Guid IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }
        public Guid IdProject
        {
            get { return idProject; }
            set { idProject = value; }
        }
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public AccessLevelCoreModel Level
        {
            get { return level; }
            set { level = value; }
        }

        public AccessCoreModel()
        {
            id = Guid.NewGuid();
            idProject = Guid.NewGuid();
            idUser = Guid.NewGuid();
            level = AccessLevelCoreModel.Manager;
            login = string.Empty;
        }

        public AccessCoreModel(Guid id, Guid idProject, Guid idUser, AccessLevelCoreModel accessLevel, string login)
        {
            this.Id = id;
            this.IdProject = idProject;
            this.IdUser = idUser;
            this.level = accessLevel;
            this.login = login;
        }
    }
}
