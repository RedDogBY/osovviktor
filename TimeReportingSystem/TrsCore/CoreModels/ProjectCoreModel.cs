﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsCore.CoreModels
{
    public class ProjectCoreModel
    {
        Guid id;
        string name;
        string description;
        DateTime startDate;
        DateTime endDate;
        List<TaskCoreModel> tasks;
        List<AccessCoreModel> access;
        Guid idAdmin;

        #region Properties
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public List<TaskCoreModel> Tasks
        {
            get { return tasks; }
            set { tasks = value; }
        }
        public List<AccessCoreModel> AccessList
        {
            get { return access; }
            set { access = value; }
        }
        public Guid IdAdmin
        {
            get { return idAdmin; }
            set { idAdmin = value; }
        }
        #endregion

        #region Constructors
        public ProjectCoreModel()
        {
            id = Guid.NewGuid();
            name = string.Empty;
            description = string.Empty;
            startDate = DateTime.Now;
            endDate = new DateTime();
            tasks = new List<TaskCoreModel>();
            this.access = new List<AccessCoreModel>();
            idAdmin = Guid.NewGuid();
        }

        public ProjectCoreModel(Guid id, Guid idAdmin)
        {
            this.id = id;
            name = string.Empty;
            description = string.Empty;
            startDate = new DateTime();
            endDate = new DateTime();
            tasks = new List<TaskCoreModel>();
            this.access = new List<AccessCoreModel>();
            this.idAdmin = idAdmin;
        }
        public ProjectCoreModel(Guid id, string name, string description, DateTime startDate, DateTime endDate, List<TaskCoreModel> tasks, List<AccessCoreModel> access, Guid idAdmin)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.startDate = startDate;
            this.endDate = endDate;
            this.tasks = tasks;
            this.access = access;
            this.idAdmin = idAdmin;
        }
        #endregion

        public override string ToString()
        {
            return String.Format("{0};{1};{2};{3};{4};{5}", id, name, description, startDate, endDate, tasks);
        }

    }
}
