﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsCore.CoreModels
{
    public class TaskReportCoreModel
    {
        Guid id;
        DateTime dateReport;
        TimeSpan effort;
        string description;
        DateTime dateCreated;
        DateTime dateEdited;
        Guid taskId;
        Guid idUser;

        #region Properties
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public DateTime DateReport
        {
            get { return dateReport; }
            set { dateReport = value; }
        }
        public TimeSpan Effort
        {
            get { return effort; }
            set { effort = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public DateTime DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }
        public DateTime DateEdited
        {
            get { return dateEdited; }
            set { dateEdited = value; }
        }
        public Guid TaskId
        {
            get { return taskId; }
            set { taskId = value; }
        }
        public Guid IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }
        #endregion

        #region Constructors
        public TaskReportCoreModel()
        {
            id = Guid.NewGuid();
            taskId = Guid.NewGuid();
            description = string.Empty;
            dateReport = new DateTime();
            effort = new TimeSpan();
            dateCreated = new DateTime();
            dateEdited = new DateTime();
            idUser = Guid.NewGuid();

        }
        public TaskReportCoreModel(Guid id, Guid taskId, Guid idUser)
        {
            this.id = id;
            this.taskId = taskId;
            this.description = string.Empty;
            this.dateReport = new DateTime();
            this.effort = new TimeSpan();
            this.dateCreated = new DateTime();
            this.dateEdited = new DateTime();
            this.idUser = idUser;

        }

        public TaskReportCoreModel(Guid id, Guid taskId, DateTime dateReport, TimeSpan effort, string description, DateTime dateCreated, DateTime dateEdited, Guid idUser)
        {
            this.id = id;
            this.taskId = taskId;
            this.dateReport = dateReport;
            this.effort = effort;
            this.description = description;
            this.dateCreated = dateCreated;
            this.dateEdited = dateEdited;
            this.idUser = idUser;
        }
        #endregion

        public override string ToString()
        {
            return String.Format("{0};{1};{2};{3};{4};{5};{6}", id, taskId, dateReport, effort, description, dateCreated, dateEdited);
        }

    }
}
