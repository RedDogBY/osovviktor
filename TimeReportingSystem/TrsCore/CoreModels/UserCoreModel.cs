﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsCore.CoreModels
{
    public class UserCoreModel
    {
        Guid id;
        string login;
        string password;
        string firstName;
        string lastName;
        DateTime registrationDate;
        string locale;
        TimeZone timeZone;
        RoleCoreModel role;


        #region Properties
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Login
        {
            get { return login; }
            set { login = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public DateTime RegistrationDate
        {
            get { return registrationDate; }
            set { registrationDate = value; }
        }
        public string Locale
        {
            get { return locale; }
            set { locale = value; }
        }
        public TimeZone TimeZone
        {
            get { return timeZone; }
            set { timeZone = value; }
        }
        public RoleCoreModel Role
        {
            get { return role; }
            set { role = value; }
        }
        #endregion

        #region Constructors
        public UserCoreModel()
        {
            id = Guid.NewGuid();
            login = string.Empty;
            password = string.Empty;
            firstName = string.Empty;
            lastName = string.Empty;
            registrationDate = DateTime.Now;
            locale = string.Empty;
            timeZone = TimeZone.CurrentTimeZone;
            role = RoleCoreModel.User;
        }

        public UserCoreModel(Guid id, string login, string password, string firstName, string lastName, DateTime registrationDate, string locale, TimeZone timeZone, RoleCoreModel role)
        {
            this.id = id;
            this.login = login;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.registrationDate = registrationDate;
            this.locale = locale;
            this.timeZone = timeZone;
            this.role = role;
        }
        #endregion

        public override string ToString()
        {
            return String.Format("{0};{1};{2};{3};{4};{5};{6};{7}", id, login, password, firstName, lastName, registrationDate, locale, timeZone);
        }
    }
}
