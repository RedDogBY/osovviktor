﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsCore.CoreModels
{
    public class TaskCoreModel
    {
        Guid id;
        string name;
        string description;
        DateTime startDate;
        DateTime endDate;
        TaskStateCoreModel state;
        List<TaskReportCoreModel> taskReports;
        Guid projectId;
        List<MappingTaskUserCoreModel> mappingTaskUsers;

        #region Properties
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public TaskStateCoreModel State
        {
            get { return state; }
            set { state = value; }
        }
        public List<TaskReportCoreModel> TaskReports
        {
            get { return taskReports; }
            set { taskReports = value; }
        }
        public Guid ProjectId
        {
            get { return projectId; }
            set { projectId = value; }
        }
        public List<MappingTaskUserCoreModel> MappingTasksUsers
        {
            get { return mappingTaskUsers; }
            set { mappingTaskUsers = value; }
        }
        #endregion

        #region Constructors
        public TaskCoreModel()
        {
            id = Guid.NewGuid();
            projectId = Guid.NewGuid();
            name = string.Empty;
            description = string.Empty;
            startDate = new DateTime();
            endDate = new DateTime();
            state = TaskStateCoreModel.Draft;
            taskReports = new List<TaskReportCoreModel>();
        }

        public TaskCoreModel(Guid id, Guid projectId)
        {
            this.id = id;
            this.projectId = projectId;
            this.name = string.Empty;
            description = string.Empty;
            startDate = new DateTime();
            endDate = new DateTime();
            state = TaskStateCoreModel.Draft;
            taskReports = new List<TaskReportCoreModel>();
        }
        public TaskCoreModel(Guid id, Guid projectId, string name, string description, DateTime startDate, DateTime endDate, TaskStateCoreModel state, List<TaskReportCoreModel> taskReports, List<MappingTaskUserCoreModel> mappingTaskUsers)
        {
            this.id = id;
            this.projectId = projectId;
            this.name = name;
            this.description = description;
            this.startDate = startDate;
            this.endDate = endDate;
            this.state = state;
            this.taskReports = taskReports;
            this.mappingTaskUsers = mappingTaskUsers;
        }
        #endregion

        public override string ToString()
        {
            return String.Format("{0};{1};{2};{3};{4};{5};{6}", id, projectId, name, description, startDate, endDate, state); ;
        }
    }
}
