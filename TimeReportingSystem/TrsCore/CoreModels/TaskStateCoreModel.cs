﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsCore.CoreModels
{
    public enum TaskStateCoreModel
    {
        Draft, Started, Completed
    }
}
