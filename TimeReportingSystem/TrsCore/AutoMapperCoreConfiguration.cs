﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using TrsDataAccess.Models;
using TrsCore.CoreModels;

namespace TrsCore
{
    class AutoMapperCoreConfiguration
    {
        public static void Configure()
        {
            Mapper.CreateMap<Project, ProjectCoreModel>();
            Mapper.CreateMap<ProjectCoreModel, Project>();
            Mapper.CreateMap<Task, TaskCoreModel>();
            Mapper.CreateMap<TaskCoreModel, Task>();
            Mapper.CreateMap<TaskReport, TaskReportCoreModel>();
            Mapper.CreateMap<TaskReportCoreModel, TaskReport>();
            Mapper.CreateMap<TaskState, TaskStateCoreModel>();
            Mapper.CreateMap<TaskStateCoreModel, TaskState>();
            Mapper.CreateMap<MappingTaskUserCoreModel, MappingTaskUser>();
            Mapper.CreateMap<MappingTaskUser, MappingTaskUserCoreModel>();
            Mapper.CreateMap<AccessCoreModel, Access>();
            Mapper.CreateMap<Access, AccessCoreModel>();
            Mapper.CreateMap<AccessLevel, AccessLevelCoreModel>();
            Mapper.CreateMap<AccessLevelCoreModel, AccessLevel>();
            Mapper.CreateMap<Role, RoleCoreModel>();
            Mapper.CreateMap<RoleCoreModel, Role>();
            Mapper.CreateMap<User, UserCoreModel>();
            Mapper.CreateMap<UserCoreModel, User>();
        }
    }
}
