﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrsCore.CoreModels;

namespace TrsCore
{
    public interface ICore
    {
        ProjectCoreModel GetProjectById(Guid id);
        IEnumerable<ProjectCoreModel> GetAllProjects();

        void AddProject(ProjectCoreModel project);
        void AddTask(TaskCoreModel task);
        void AddTaskReport(TaskReportCoreModel taskReport);

        void RemoveProject(ProjectCoreModel project);
        void RemoveTask(TaskCoreModel task);
        void RemoveTask(Guid idTask, Guid idProject);
        void RemoveTaskReport(TaskReportCoreModel taskReport);
        void RemoveTaskReport(Guid idReport, Guid idTask, Guid idProject);

        void ModifyProject(ProjectCoreModel project);
        void ModifyTask(TaskCoreModel task);
        void ModifyTaskReport(TaskReportCoreModel taskReport);

        UserCoreModel GetUserById(Guid id);
        UserCoreModel GetUserByLogin(string login);
        IEnumerable<UserCoreModel> GetAllUsers();
        IEnumerable<UserCoreModel> GetUsersByProject(ProjectCoreModel project);
        IEnumerable<UserCoreModel> GetUnassignedUser(ProjectCoreModel project);       //Получение неназначенных юзеров на этот проект
        IEnumerable<UserCoreModel> GetUsersByTask(TaskCoreModel task);
        IEnumerable<ProjectCoreModel> GetProjectByUser(Guid idAdmin);                //Получение проектов админа 

        IEnumerable<ProjectCoreModel> GetAccessibleProjects(Guid idUser);  //Получить доступные проекты.

        void AddUser(UserCoreModel user);
        void RemoveUser(UserCoreModel user);
        void ModifyUser(UserCoreModel user);


        bool ExistsUser(string login);
        bool ValidateUser(string login, string password);

        AccessLevelCoreModel GetAccessLevel(ProjectCoreModel project, Guid idUser);                              //Получение уровеня доступа на проект у юзера
        AccessLevelCoreModel GetAccessLevel(ProjectCoreModel project, string login);
        void AssignUserOnProject(Guid idProject, Guid idUser, AccessLevelCoreModel accessLevel, string login);   //Назначение юзера на проект

        IEnumerable<UserCoreModel> GetUnassignedUserOnTask(TaskCoreModel task, Guid idUser);          //Получение мемберов из назначенных на проект, но неназначенных на эту задачу
        void AssignUserOnTask(Guid idProject, Guid idTask, Guid idUser);                               //Назначение юзера на задачу

        IEnumerable<TaskCoreModel> GetTasks(Guid idProject, Guid idUser);                          //Получение списка задач в соответствии с доступом
        IEnumerable<TaskReportCoreModel> GetReports(Guid idProject, Guid idTask, Guid idUser);      //Получение списка репортов в соответствии с доступом

        void RemoveUserOnTask(Guid idUser, Guid idTask, Guid idProject);
        void ModifyAssignedUser(AccessCoreModel access);
        void RemoveAssignedUser(Guid idProject, Guid idAccess);



    }
}
