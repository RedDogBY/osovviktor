﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using TrsCore.CoreModels;
using TrsDataAccess;
using TrsDataAccess.Models;

namespace TrsCore
{
    public class Core : ICore
    {
        private readonly string fileNameProjectRepository;
        private readonly string fileNameUserRepository;

        public Core(string filenameProjectRepository, string fileNameUserRepository)
        {
            AutoMapperCoreConfiguration.Configure();
            this.fileNameProjectRepository = filenameProjectRepository;
            this.fileNameUserRepository = fileNameUserRepository;
        }

        private IRepository<Project> GetRepository()
        {
            return new JsonRepository(fileNameProjectRepository);
        }

        private IRepository<User> GetUserRepository()
        {
            return new UserRepository(fileNameUserRepository);
        }

        public ProjectCoreModel GetProjectById(Guid id)
        {
            ProjectCoreModel project = Mapper.Map<Project, ProjectCoreModel>(GetRepository().GetById(id));
            return project;
        }

        public IEnumerable<ProjectCoreModel> GetAllProjects()
        {
            return Mapper.Map<IEnumerable<Project>, IEnumerable<ProjectCoreModel>>(GetRepository().GetAll());
        }

        #region Add
        public void AddProject(ProjectCoreModel project)
        {
            GetRepository().Add(Mapper.Map<ProjectCoreModel, Project>(project));
        }
        public void AddTask(TaskCoreModel task)
        {
            ProjectCoreModel p = Mapper.Map<Project, ProjectCoreModel>(GetRepository().GetById(task.ProjectId));
            p.Tasks.Add(task);
            ModifyProject(p);

        }
        public void AddTaskReport(TaskReportCoreModel taskReport)
        {
            ProjectCoreModel p = Mapper.Map<Project, ProjectCoreModel>(GetRepository().GetAll().First(x => x.Tasks.Exists(y => y.Id.Equals(taskReport.TaskId))));
            p.Tasks.First(x => x.Id.Equals(taskReport.TaskId)).TaskReports.Add(taskReport);
            ModifyProject(p);
        }
        #endregion

        #region Remove
        public void RemoveProject(ProjectCoreModel project)
        {
            GetRepository().Remove(Mapper.Map<ProjectCoreModel, Project>(project));
        }
        public void RemoveTask(TaskCoreModel task)
        {
            ProjectCoreModel p = Mapper.Map<Project, ProjectCoreModel>(GetRepository().GetById(task.ProjectId));
            List<TaskCoreModel> list = p.Tasks;
            list.Remove(list.Find(x => x.Id.Equals(task.Id)));
            ModifyProject(p);
        }
        public void RemoveTask(Guid idTask, Guid idProject)
        {
            ProjectCoreModel p = Mapper.Map<Project, ProjectCoreModel>(GetRepository().GetById(idProject));
            List<TaskCoreModel> list = p.Tasks;
            list.Remove(list.Find(x => x.Id.Equals(idTask)));
            ModifyProject(p);
        }
        public void RemoveTaskReport(TaskReportCoreModel taskReport)
        {
            ProjectCoreModel p = Mapper.Map<Project, ProjectCoreModel>(GetRepository().GetAll().First(x => x.Tasks.Exists(y => y.Id.Equals(taskReport.TaskId))));
            List<TaskReportCoreModel> list = p.Tasks.First(x => x.Id.Equals(taskReport.TaskId)).TaskReports;
            list.Remove(list.Find(x => x.Id.Equals(taskReport.Id)));
            ModifyProject(p);
        }
        public void RemoveTaskReport(Guid idReport, Guid idTask, Guid idProject)
        {
            ProjectCoreModel project = GetProjectById(idProject);
            List<TaskReportCoreModel> list = project.Tasks.Find(x => x.Id.Equals(idTask)).TaskReports;
            list.Remove(list.Find(x => x.Id.Equals(idReport)));
            ModifyProject(project);
        }
        #endregion

        #region Modify
        public void ModifyProject(ProjectCoreModel project)
        {
            GetRepository().Modify(Mapper.Map<ProjectCoreModel, Project>(project));
        }
        public void ModifyTask(TaskCoreModel task)
        {
            ProjectCoreModel p = Mapper.Map<Project, ProjectCoreModel>(GetRepository().GetById(task.ProjectId));
            TaskCoreModel t = p.Tasks.First(x => x.Id.Equals(task.Id));
            t.Name = task.Name;
            t.Description = task.Description;
            t.StartDate = task.StartDate;
            t.EndDate = task.EndDate;
            t.State = task.State;
            t.TaskReports = task.TaskReports;
            t.MappingTasksUsers = task.MappingTasksUsers;

            ModifyProject(p);
        }
        public void ModifyTaskReport(TaskReportCoreModel taskReport)
        {
            ProjectCoreModel p = Mapper.Map<Project, ProjectCoreModel>(GetRepository().GetAll().First(x => x.Tasks.Exists(y => y.Id.Equals(taskReport.TaskId))));
            TaskReportCoreModel tr = p.Tasks.First(x => x.Id.Equals(taskReport.TaskId)).TaskReports.First(y => y.Id.Equals(taskReport.Id));
            tr.DateCreated = taskReport.DateCreated;
            tr.DateEdited = DateTime.UtcNow;
            tr.DateReport = taskReport.DateReport;
            tr.Effort = taskReport.Effort;
            tr.Description = taskReport.Description;
            tr.IdUser = taskReport.IdUser;
            ModifyProject(p);
        }
        #endregion

        #region For user

        public void AddUser(UserCoreModel user)
        {
            GetUserRepository().Add(Mapper.Map<UserCoreModel, User>(user));
        }

        public void RemoveUser(UserCoreModel user)
        {
            GetUserRepository().Remove(Mapper.Map<UserCoreModel, User>(user));
        }

        public void ModifyUser(UserCoreModel user)
        {
            GetUserRepository().Modify(Mapper.Map<UserCoreModel, User>(user));
        }
        #endregion

        public UserCoreModel GetUserById(Guid id)
        {
            return Mapper.Map<User, UserCoreModel>(GetUserRepository().GetById(id));
        }

        public UserCoreModel GetUserByLogin(string login)
        {
            return GetAllUsers().ToList().Find(x => x.Login == login);
        }

        public IEnumerable<UserCoreModel> GetAllUsers()
        {
            return Mapper.Map<IEnumerable<User>, IEnumerable<UserCoreModel>>(GetUserRepository().GetAll());
        }

        public IEnumerable<UserCoreModel> GetUsersByProject(ProjectCoreModel project)
        {
            List<UserCoreModel> tmp = new List<UserCoreModel>();
            List<UserCoreModel> list = GetAllUsers().ToList();
            foreach (AccessCoreModel a in project.AccessList)
            {
                tmp.Add(list.First(x => x.Id == a.IdUser));
            }
            return tmp;
        }
        public IEnumerable<UserCoreModel> GetUsersByTask(TaskCoreModel task)
        {
            List<UserCoreModel> tmp = new List<UserCoreModel>();
            List<UserCoreModel> allUsers = GetAllUsers().ToList();
            foreach (MappingTaskUserCoreModel taskUser in task.MappingTasksUsers)
            {
                tmp.Add(allUsers.First(x => x.Id == taskUser.IdUser));
            }
            return tmp;
        }

        public IEnumerable<UserCoreModel> GetUnassignedUser(ProjectCoreModel project)
        {
            List<UserCoreModel> tmp = new List<UserCoreModel>();
            List<UserCoreModel> allUsers = GetAllUsers().ToList();
            foreach (UserCoreModel user in allUsers)
            {
                if (!project.AccessList.Exists(x => x.IdUser.Equals(user.Id)) && !user.Id.Equals(project.IdAdmin))
                    tmp.Add(user);
            }
            return tmp;
        }

        public bool ExistsUser(string login)
        {
            return GetAllUsers().Any(x => x.Login == login);
        }
        public bool ValidateUser(string login, string password)
        {
            return GetAllUsers().Any(x => x.Login == login && x.Password == password);
        }
        public AccessLevelCoreModel GetAccessLevel(ProjectCoreModel project, Guid idUser)
        {
            return project.AccessList.Find(x => x.IdUser.Equals(idUser)).Level;
        }
        public AccessLevelCoreModel GetAccessLevel(ProjectCoreModel project, string login)
        {
            return project.AccessList.Find(x => x.Login == login).Level;
        }

        public IEnumerable<ProjectCoreModel> GetProjectByUser(Guid idAdmin)
        {
            return GetAllProjects().Where(x => x.IdAdmin == idAdmin);
        }

        public void AssignUserOnProject(Guid idProject, Guid idUser, AccessLevelCoreModel accessLevel, string login)
        {
            ProjectCoreModel project = GetProjectById(idProject);
            project.AccessList.Add(new AccessCoreModel(Guid.NewGuid(), idProject, idUser, accessLevel, login));
            ModifyProject(project);
        }

        public IEnumerable<ProjectCoreModel> GetAccessibleProjects(Guid idUser)
        {
            List<ProjectCoreModel> tmp = new List<ProjectCoreModel>();
            foreach (ProjectCoreModel p in GetAllProjects())
            {
                if (p.AccessList.Exists(x => x.IdUser == idUser))
                    tmp.Add(p);
            }
            return tmp;
        }

        public IEnumerable<UserCoreModel> GetUnassignedUserOnTask(TaskCoreModel task, Guid idUser)
        {
            List<UserCoreModel> tmp = new List<UserCoreModel>();
            ProjectCoreModel project = GetProjectById(task.ProjectId);
            List<UserCoreModel> users = GetAllUsers().ToList();
            foreach (AccessCoreModel access in project.AccessList)
            {
                if (!task.MappingTasksUsers.Exists(x => x.IdUser.Equals(access.IdUser)) && !access.IdUser.Equals(idUser) && access.Level != AccessLevelCoreModel.Manager)
                    tmp.Add(users.Find(x => x.Id.Equals(access.IdUser)));
            }
            return tmp;

        }
        public void AssignUserOnTask(Guid idProject, Guid idTask, Guid idUser)
        {
            TaskCoreModel task = GetProjectById(idProject).Tasks.Find(x => x.Id.Equals(idTask));
            task.MappingTasksUsers.Add(new MappingTaskUserCoreModel(Guid.NewGuid(), idTask, idUser, GetUserById(idUser).Login));
            ModifyTask(task);
        }

        public IEnumerable<TaskCoreModel> GetTasks(Guid idProject, Guid idUser)
        {
            List<TaskCoreModel> tmp = new List<TaskCoreModel>();
            ProjectCoreModel project = GetProjectById(idProject);
            AccessLevelCoreModel access = GetAccessLevel(project, idUser);
            if (access.Equals(AccessLevelCoreModel.Manager))
                tmp = project.Tasks;
            else
                tmp = project.Tasks.Where(x => x.MappingTasksUsers.Exists(y => y.IdUser.Equals(idUser))).ToList();
            return tmp;
        }
        public IEnumerable<TaskReportCoreModel> GetReports(Guid idProject, Guid idTask, Guid idUser)
        {
            List<TaskReportCoreModel> tmp = new List<TaskReportCoreModel>();
            ProjectCoreModel project = GetProjectById(idProject);
            AccessLevelCoreModel access = GetAccessLevel(project, idUser);
            if (access.Equals(AccessLevelCoreModel.Manager))
                tmp = project.Tasks.Find(x => x.Id.Equals(idTask)).TaskReports;
            else
                tmp = project.Tasks.Find(x => x.Id.Equals(idTask)).TaskReports.Where(y => y.IdUser.Equals(idUser)).ToList();
            return tmp;
        }

        public void RemoveUserOnTask(Guid idUser, Guid idTask, Guid idProject)
        {
            ProjectCoreModel project = GetProjectById(idProject);
            List<MappingTaskUserCoreModel> list = project.Tasks.Find(x => x.Id.Equals(idTask)).MappingTasksUsers;
            list.Remove(list.Find(x => x.IdUser.Equals(idUser)));
            ModifyProject(project);
        }

        public void ModifyAssignedUser(AccessCoreModel access)
        {
            ProjectCoreModel project = GetProjectById(access.IdProject);
            AccessCoreModel tmp = project.AccessList.Find(x => x.Id.Equals(access.Id));
            tmp.Level = access.Level;
            ModifyProject(project);
        }

        public void RemoveAssignedUser(Guid idProject, Guid idAccess)
        {
            ProjectCoreModel project = GetProjectById(idProject);
            project.AccessList.Remove(project.AccessList.Find(x => x.Id.Equals(idAccess)));
            ModifyProject(project);
        }
    }
}
