﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using TrsCore.CoreModels;

namespace ConsoleTimeReportingSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            List<Photo> PhotoUser = new List<Photo>();
            PhotoUser.Add(new Photo() {Id=1,Name = "Первая фотка"});
            PhotoUser.Add(new Photo() { Id = 1, Name = "Вторая герман фотка" });
            var user1 = new User()
            {
                Id = 404,
                Email = "chernikov@gmail.com",
                UserName = "rollinx",
                Name = "Andrey",
                FirstName = "Andrey",
                MiddleName = "Alexandrovich",
                LastName = "Chernikov",
                Gender = User.GenderEnum.Male,
                PhotoAlbum = PhotoUser
            };
            var user2 = new User()
            {
                Id = 402,
                Email = "chernikov@gmail.com2",
                UserName = "rollinx2",
                Name = "Andrey2",
                FirstName = "Andrey2",
                MiddleName = "Alexandrovich2",
                LastName = "Chernikov",
                Gender = User.GenderEnum.Male,
                PhotoAlbum = PhotoUser
            };
            List<User> Users = new List<User>();
            Users.Add(user1);
            Users.Add(user2);

            string jsonUser = JsonConvert.SerializeObject(Users);
            System.Console.Write(jsonUser);

            List<User> userD = JsonConvert.DeserializeObject<List<User>>(jsonUser);

            var jsonUserSource = "{\"Id\":\"405\",\"Name\":\"Andrey\",\"FirstName\":\"Andrey\",\"MiddleName\":\"Alexandrovich\",\"LastName\":\"Chernikov\",\"UserName\":\"rollinx\",\"Gender\":\"M\",\"Email\":\"chernikov@gmail.com\"}";
            var user3 = JsonConvert.DeserializeObject<User>(jsonUserSource);
            */
            TrsCore.Core core = new TrsCore.Core("db.json", "users.json");
            UserCoreModel admin = new UserCoreModel(Guid.NewGuid(), "Admin", "Admin", "Osov", "Viktor", DateTime.Now, "ru-RU", TimeZone.CurrentTimeZone, RoleCoreModel.Admin);
            UserCoreModel member = new UserCoreModel(Guid.NewGuid(), "RedDog", "123", "Osov", "Viktor", DateTime.Now, "ru-RU", TimeZone.CurrentTimeZone, RoleCoreModel.User);

            List<TaskCoreModel> ListTasks = new List<TaskCoreModel>();
            List<AccessCoreModel> ListAccess = new List<AccessCoreModel>();
            List<MappingTaskUserCoreModel> ListTasksUserst1 = new List<MappingTaskUserCoreModel>();
            List<MappingTaskUserCoreModel> ListTasksUserst2 = new List<MappingTaskUserCoreModel>();

            ProjectCoreModel project = new ProjectCoreModel(Guid.NewGuid(), "OsovProject", "First Project", DateTime.Now, DateTime.Now, new List<TaskCoreModel>(), new List<AccessCoreModel>(), member.Id);
            ProjectCoreModel projectA = new ProjectCoreModel(Guid.NewGuid(), "AdminProject", "Admin Project", DateTime.Now, DateTime.Now, new List<TaskCoreModel>(), new List<AccessCoreModel>(), admin.Id);
            core.AddUser(admin);
            core.AddUser(member);
            TaskCoreModel t1 = new TaskCoreModel(Guid.NewGuid(),project.Id,"Task1","Opisanie1",DateTime.Now,DateTime.Now,TaskStateCoreModel.Started,new List<TaskReportCoreModel>(), new List<MappingTaskUserCoreModel>());
            TaskCoreModel t2 = new TaskCoreModel(Guid.NewGuid(), project.Id, "Task2", "Opisanie2", DateTime.Now, DateTime.Now, TaskStateCoreModel.Draft, new List<TaskReportCoreModel>(), new List<MappingTaskUserCoreModel>());
            AccessCoreModel a1 = new AccessCoreModel(Guid.NewGuid(),project.Id,member.Id,AccessLevelCoreModel.Member,"RedDog");
            AccessCoreModel a2 = new AccessCoreModel(Guid.NewGuid(), projectA.Id, admin.Id, AccessLevelCoreModel.Manager, "Admin");
            MappingTaskUserCoreModel m1 = new MappingTaskUserCoreModel(Guid.NewGuid(),t1.Id,member.Id,"RedDog");
            ListTasksUserst1.Add(m1);
            MappingTaskUserCoreModel m2 = new MappingTaskUserCoreModel(Guid.NewGuid(), t2.Id, member.Id, "RedDog");
            MappingTaskUserCoreModel m2a = new MappingTaskUserCoreModel(Guid.NewGuid(), t2.Id, admin.Id, "Admin");
            ListTasksUserst2.Add(m2);
            ListTasksUserst2.Add(m2a);
            t1.MappingTasksUsers = ListTasksUserst1;
            t2.MappingTasksUsers = ListTasksUserst2;
            ListTasks.Add(t1);
            ListTasks.Add(t2);
            project.Tasks = ListTasks;
            ListAccess.Add(a1);
            ListAccess.Add(a2);
            project.AccessList = ListAccess;

            core.AddProject(project);

            System.Console.ReadLine();
        }
    }
}
