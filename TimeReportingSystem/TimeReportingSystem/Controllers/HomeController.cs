﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using TimeReportingSystem.Models;
using TrsCore;
using TrsCore.CoreModels;

namespace TimeReportingSystem.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        Core CE = new Core(ReadConfigData.LocationProjects, ReadConfigData.LocationUsers);
        public ActionResult Index()
        {
            IEnumerable<ProjectViewModel> projectall = Mapper.Map<IEnumerable<ProjectCoreModel>, IEnumerable<ProjectViewModel>>(CE.GetAllProjects());
            return View(projectall);
        }

    }
}
