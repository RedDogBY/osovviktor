﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using TimeReportingSystem.Models;
using TrsCore;
using TrsCore.CoreModels;

namespace TimeReportingSystem.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        public static Core Core = new Core(ReadConfigData.LocationProjects, ReadConfigData.LocationUsers);

        public ActionResult ReportList(Guid idTask, Guid idProject)
        {
            ViewBag.idTask = idTask;
            ViewBag.idProject = idProject;
            Session["ID"] = Core.GetUserByLogin(User.Identity.Name).Id;
            return View(Mapper.Map<IEnumerable<TaskReportCoreModel>, IEnumerable<TaskReportViewModel>>(Core.GetReports(idProject, idTask, (Guid)Session["ID"])));
        }
        [HttpGet]
        public ActionResult AddReport(Guid idProject, Guid idTask)
        {
            ViewBag.idTask = idTask;
            ViewBag.idProject = idProject;
            return View(new TaskReportViewModel(Guid.NewGuid(), idTask, (Guid)Session["ID"]));
        }

        [HttpPost]
        public ActionResult AddReport(TaskReportViewModel taskReport)
        {
            taskReport.DateCreated = DateTime.UtcNow;
            taskReport.DateEdited = DateTime.UtcNow;
            Core.AddTaskReport(Mapper.Map<TaskReportViewModel, TaskReportCoreModel>(taskReport));
            return RedirectToAction("ProjectList", "Project");
        }

        [HttpGet]
        public ActionResult EditReport(Guid idReport, Guid idTask, Guid idProject)
        {
            ViewBag.idTask = idTask;
            ViewBag.idProject = idProject;
            TaskReportViewModel taskReport = Mapper.Map<TaskReportCoreModel, TaskReportViewModel>(Core.GetProjectById(idProject).Tasks.Find(x => x.Id.Equals(idTask)).TaskReports.Find(y => y.Id.Equals(idReport)));
            return View(taskReport);
        }
        [HttpPost]
        public ActionResult EditReport(TaskReportViewModel taskReport)
        {
            taskReport.DateEdited = DateTime.UtcNow;
            Core.ModifyTaskReport(Mapper.Map<TaskReportViewModel, TaskReportCoreModel>(taskReport));
            return RedirectToAction("ProjectList", "Project");
        }
        public ActionResult DeleteReport(Guid idReport, Guid idTask, Guid idProject)
        {
            Core.RemoveTaskReport(idReport, idTask, idProject);
            return RedirectToAction("ReportList", "Report", new { idTask = idTask, idProject = idProject });
        }

    }
}
