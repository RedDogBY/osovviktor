﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using TimeReportingSystem.Models;
using TrsCore;
using TrsCore.CoreModels;

namespace TimeReportingSystem.Controllers
{
    public class TaskController : Controller
    {
        //
        // GET: /Task/
        public static Core Core = new Core(ReadConfigData.LocationProjects, ReadConfigData.LocationUsers);
        public ActionResult TaskList(Guid idProject)
        {
            ViewBag.idProject = idProject;
            UserViewModel user = Mapper.Map<UserCoreModel, UserViewModel>(Core.GetUserByLogin(User.Identity.Name));
            Session["ID"] = user.Id;
            Session["Access"] = Core.GetAccessLevel(Core.GetProjectById(idProject), user.Id);
            return View(Mapper.Map<IEnumerable<TaskCoreModel>, IEnumerable<TaskViewModel>>(Core.GetTasks(idProject, (Guid)Session["ID"])));
        }
        [HttpGet]
        public ActionResult AddTask(Guid idProject)
        {
            return View(new TaskViewModel(Guid.NewGuid(), idProject));
        }

        [HttpPost]
        public ActionResult AddTask(TaskViewModel task)
        {

            task.MappingTaskUsers = new List<MappingTaskUserViewModel>();
            task.StartDate = DateTime.UtcNow;
            Core.AddTask(Mapper.Map<TaskViewModel, TaskCoreModel>(task));
            return RedirectToAction("TaskList", "Task", new { idProject = task.ProjectId });
        }
        [HttpGet]
        public ActionResult EditTask(Guid idTask, Guid idProject)
        {
            return View(Mapper.Map<TaskCoreModel, TaskViewModel>(Core.GetProjectById(idProject).Tasks.Find(x => x.Id.Equals(idTask))));
        }
        [HttpPost]
        public ActionResult EditTask(TaskViewModel task)
        {
            if (ModelState.IsValid)
            {
                Core.ModifyTask(Mapper.Map<TaskViewModel, TaskCoreModel>(task));
                return RedirectToAction("TaskList", "Task", new { idProject = task.ProjectId });
            }
            return View(task);
        }
        public ActionResult DeleteTask(Guid idTask, Guid idProject)
        {
            Core.RemoveTask(idTask, idProject);
            return RedirectToAction("TaskList", "Task", new { idProject = idProject });
        }
    }
}
