﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using TimeReportingSystem.Models;
using TrsCore;
using TrsCore.CoreModels;

namespace TimeReportingSystem.Controllers
{
    //[Authorize]
    public class ProjectController : Controller
    {
        //
        // GET: /Project/
        public Core Core = new Core(ReadConfigData.LocationProjects, ReadConfigData.LocationUsers);
        public ActionResult ProjectList()
        {
            UserViewModel user = Mapper.Map<UserCoreModel, UserViewModel>(Core.GetUserByLogin(HttpContext.User.Identity.Name));
            Session["ID"] = user.Id;
            Session["Role"] = user.Role;
            return View(Mapper.Map<IEnumerable<ProjectCoreModel>, IEnumerable<ProjectViewModel>>(Core.GetAccessibleProjects((Guid)Session["ID"])));
        }

        public ActionResult MyProjects()
        {
            Session["ID"] = Core.GetUserByLogin(User.Identity.Name).Id;
            return View(Mapper.Map<IEnumerable<ProjectCoreModel>, IEnumerable<ProjectViewModel>>(Core.GetProjectByUser((Guid)Session["ID"])));
        }
        [HttpGet]
        public ActionResult AddProject()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddProject(ProjectViewModel project)
        {

            project.Id = Guid.NewGuid();
            project.Tasks = new List<TaskViewModel>();
            project.AccessList = new List<AccessViewModel>();
            project.IdAdmin = (Guid)Session["ID"];
            project.StartDate = DateTime.UtcNow;
            Core.AddProject(Mapper.Map<ProjectViewModel, ProjectCoreModel>(project));
            return RedirectToAction("MyProjects", "Project");
        }
        [HttpGet]
        public ActionResult EditProject(Guid idProject)
        {
            return View(Mapper.Map<ProjectCoreModel, ProjectViewModel>(Core.GetProjectById(idProject)));
        }
        [HttpPost]
        public ActionResult EditProject(ProjectViewModel project)
        {
            if (ModelState.IsValid)
            {
                Core.ModifyProject(Mapper.Map<ProjectViewModel, ProjectCoreModel>(project));
                return RedirectToAction("MyProjects", "Project");
            }
            return View(project);
        }

        public ActionResult RemoveProject(Guid idProject)
        {
            Core.RemoveProject(Core.GetProjectById(idProject));
            return RedirectToAction("MyProjects", "Project");
        }

    }
}
