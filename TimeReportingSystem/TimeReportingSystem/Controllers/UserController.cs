﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using TimeReportingSystem.Models;
using TrsCore;
using TrsCore.CoreModels;

namespace TimeReportingSystem.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        public static Core Core = new Core(ReadConfigData.LocationProjects, ReadConfigData.LocationUsers);

        public ActionResult UserList(Guid idProject)
        {
            ViewBag.IdProject = idProject;
            IEnumerable<AccessViewModel> access = Mapper.Map<IEnumerable<AccessCoreModel>, IEnumerable<AccessViewModel>>(Core.GetProjectById(idProject).AccessList);
            return View(access);
        }

        [HttpGet]
        public ActionResult AddUser(Guid idProject)
        {
            ViewBag.idProject = idProject;

            return View(Mapper.Map<IEnumerable<UserCoreModel>, IEnumerable<UserViewModel>>(Core.GetUnassignedUser(Core.GetProjectById(idProject))));
        }

        public ActionResult AssignedUser(Guid idProject, Guid idUser, AccessLevelViewModel accessLevel, string login)
        {
            Core.AssignUserOnProject(idProject, idUser, Mapper.Map<AccessLevelViewModel, AccessLevelCoreModel>(accessLevel), login);
            return RedirectToAction("UserList", "User", new { idProject = idProject });
        }

        [HttpGet]
        public ActionResult CreateUser(Guid idProject)
        {
            ViewBag.idProject = idProject;
            return View();
        }
        [HttpPost]
        public ActionResult CreateUser(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Core.ExistsUser(model.Login))
                {
                    model.Id = Guid.NewGuid();
                    model.RegistrationDate = DateTime.UtcNow;
                    Core.AddUser(Mapper.Map<UserViewModel, UserCoreModel>(model));
                    return RedirectToAction("ProjectList", "Project");
                }
                else
                    ModelState.AddModelError("", "Логин занят");
            }
            return View(model);
        }

        public ActionResult UsersOnTask(Guid idTask, Guid idProject)
        {
            ViewBag.idProject = idProject;
            ViewBag.idTask = idTask;
            Session["ID"] = Core.GetUserByLogin(User.Identity.Name).Id;
            return View(Mapper.Map<IEnumerable<UserCoreModel>, IEnumerable<UserViewModel>>(Core.GetUsersByTask(Core.GetProjectById(idProject).Tasks.Find(x => x.Id.Equals(idTask)))));
        }

        public ActionResult UnassignedUserOnTask(Guid idTask, Guid idProject)
        {
            ViewBag.idTask = idTask;
            ViewBag.idProject = idProject;
            return View(Mapper.Map<IEnumerable<UserCoreModel>, IEnumerable<UserViewModel>>(Core.GetUnassignedUserOnTask(Core.GetProjectById(idProject).Tasks.Find(x => x.Id == idTask), (Guid)Session["ID"])));
        }


        public ActionResult AddUserOnTask(Guid idUser, Guid idTask, Guid idProject)
        {
            Core.AssignUserOnTask(idProject, idTask, idUser);
            return RedirectToAction("ProjectList", "Project");
        }
        public ActionResult RemoveUserFromTask(Guid idUser, Guid idTask, Guid idProject)
        {
            Core.RemoveUserOnTask(idUser, idTask, idProject);
            return RedirectToAction("UsersOnTask", "User", new { idTask = idTask, idProject = idProject });
        }
        [HttpGet]
        public ActionResult EditRoleUser(Guid idAccess, Guid idProject)
        {
            return View(Mapper.Map<AccessCoreModel, AccessViewModel>(Core.GetProjectById(idProject).AccessList.Find(x => x.Id.Equals(idAccess))));
        }
        [HttpPost]
        public ActionResult EditRoleUser(AccessViewModel access)
        {
            Core.ModifyAssignedUser(Mapper.Map<AccessViewModel, AccessCoreModel>(access));
            return RedirectToAction("UserList", "User", new { idProject = access.IdProject });
        }

        public ActionResult RemoveUserFromProject(Guid idProject, Guid idAccess)
        {
            Core.RemoveAssignedUser(idProject, idAccess);
            return RedirectToAction("UserList", "User", new { idProject = idProject });
        }
    }
}
