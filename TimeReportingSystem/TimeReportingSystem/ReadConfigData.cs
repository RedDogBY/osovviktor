﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeReportingSystem
{
    public static class ReadConfigData
    {
        public static readonly string LocationProjects;
        public static readonly string LocationUsers;

        static ReadConfigData()
        {
            System.Configuration.Configuration WebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/");

            System.Configuration.KeyValueConfigurationElement projects = WebConfig.AppSettings.Settings["Projects"];
            System.Configuration.KeyValueConfigurationElement users = WebConfig.AppSettings.Settings["Users"];
            LocationProjects = HttpContext.Current.Request.PhysicalApplicationPath + projects.Value;
            LocationUsers = HttpContext.Current.Request.PhysicalApplicationPath + users.Value;
        }
        
    }
}