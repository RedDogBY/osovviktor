﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TimeReportingSystem.Models;
using TrsCore.CoreModels;

namespace TimeReportingSystem
{
    public class AutoMapperViewConfiguration
    {
        public static void Configure()
        {
            Mapper.CreateMap<ProjectViewModel, ProjectCoreModel>();
            Mapper.CreateMap<ProjectCoreModel, ProjectViewModel>();
            Mapper.CreateMap<TaskViewModel, TaskCoreModel>();
            Mapper.CreateMap<TaskCoreModel, TaskViewModel>();
            Mapper.CreateMap<TaskReportViewModel, TaskReportCoreModel>();
            Mapper.CreateMap<TaskReportCoreModel, TaskReportViewModel>();
            Mapper.CreateMap<TaskStateViewModel, TaskStateCoreModel>();
            Mapper.CreateMap<TaskStateCoreModel, TaskStateViewModel>();
            Mapper.CreateMap<MappingTaskUserViewModel, MappingTaskUserCoreModel>();
            Mapper.CreateMap<MappingTaskUserCoreModel, MappingTaskUserViewModel>();
            Mapper.CreateMap<AccessViewModel, AccessCoreModel>();
            Mapper.CreateMap<AccessCoreModel, AccessViewModel>();
            Mapper.CreateMap<AccessLevelViewModel, AccessLevelCoreModel>();
            Mapper.CreateMap<AccessLevelCoreModel, AccessLevelViewModel>();
            Mapper.CreateMap<RoleViewModel, RoleCoreModel>();
            Mapper.CreateMap<RoleCoreModel, RoleViewModel>();
            Mapper.CreateMap<UserViewModel, UserCoreModel>();
            Mapper.CreateMap<UserCoreModel, UserViewModel>();
        }
    }
}