﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using AutoMapper;
using TimeReportingSystem.Models;
using TrsCore;
using TrsCore.CoreModels;

namespace TimeReportingSystem.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        Core CE = new Core(@"E:\EPAM\TrainingProjects\TimeReportingSystem\TimeReportingSystem\App_Data\db.json", @"E:\EPAM\TrainingProjects\TimeReportingSystem\TimeReportingSystem\App_Data\users.json");

        public override string[] GetRolesForUser(string username)
        {
            string[] role = new string[] { };

                // Получаем пользователя
                UserViewModel user = Mapper.Map<UserCoreModel, UserViewModel>(CE.GetUserByLogin(username));

                // получаем роль
                role = new string[] { Convert.ToString(user.Role) };

            return role;
        }
        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }
        public override bool IsUserInRole(string username, string roleName)
        {

            bool outputResult = false;
            // Находим пользователя
                try
                {
                    // Получаем пользователя
                    UserViewModel user = Mapper.Map<UserCoreModel, UserViewModel>(CE.GetUserByLogin(username));
                    if (user != null)
                    {
                        //сравниваем
                        if (Convert.ToString(user.Role) == roleName)
                        {
                            outputResult = true;
                        }
                    }
                }
                catch
                {
                    outputResult = false;
                }
            return outputResult;
        }
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}