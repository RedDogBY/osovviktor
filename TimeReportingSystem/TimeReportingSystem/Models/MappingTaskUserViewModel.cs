﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeReportingSystem.Models
{
    public class MappingTaskUserViewModel
    {
        Guid id;
        Guid idTask;
        Guid idUser;
        string login;

        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        public Guid IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }
        public Guid IdProject
        {
            get { return idTask; }
            set { idTask = value; }
        }
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        public MappingTaskUserViewModel()
        {
            id = Guid.NewGuid();
            idTask = Guid.NewGuid();
            idUser = Guid.NewGuid();
            login = string.Empty;
        }
        public MappingTaskUserViewModel(Guid id, Guid idTask, Guid idUser, string login)
        {
            this.Id = id;
            this.IdProject = idTask;
            this.IdUser = idUser;
            this.login = login;
        }
    }
}