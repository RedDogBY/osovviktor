﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeReportingSystem.Models
{
    public class AccessViewModel
    {
        Guid id;
        Guid idProject;
        Guid idUser;
        AccessLevelViewModel level;
        string login;

        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        public Guid IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }
        public Guid IdProject
        {
            get { return idProject; }
            set { idProject = value; }
        }
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public AccessLevelViewModel Level
        {
            get { return level; }
            set { level = value; }
        }

        public AccessViewModel()
        {
            id = Guid.NewGuid();
            idProject = Guid.NewGuid();
            idUser = Guid.NewGuid();
            level = AccessLevelViewModel.Manager;
            login = string.Empty;
        }
        public AccessViewModel(Guid id, Guid idProject, Guid idUser, AccessLevelViewModel accessLevel, string login)
        {
            this.Id = id;
            this.IdProject = idProject;
            this.IdUser = idUser;
            this.level = accessLevel;
            this.login = login;
        }
    }
}