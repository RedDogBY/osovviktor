﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeReportingSystem.Models
{
    public enum TaskStateViewModel
    {
        Draft, Started, Completed
    }
}