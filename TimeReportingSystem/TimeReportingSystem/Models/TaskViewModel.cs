﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeReportingSystem.Models
{
    public class TaskViewModel
    {
        Guid id;
        string name;
        string description;
        DateTime startDate;
        DateTime endDate;
        TaskStateViewModel state;
        List<TaskReportViewModel> taskReports;
        Guid projectId;
        List<MappingTaskUserViewModel> mappingTaskUsers;

        #region Properties
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public TaskStateViewModel State
        {
            get { return state; }
            set { state = value; }
        }
        public List<TaskReportViewModel> TaskReports
        {
            get { return taskReports; }
            set { taskReports = value; }
        }
        public Guid ProjectId
        {
            get { return projectId; }
            set { projectId = value; }
        }
        public List<MappingTaskUserViewModel> MappingTaskUsers
        {
            get { return mappingTaskUsers; }
            set { mappingTaskUsers = value; }
        }
        #endregion

        #region Constructors
        public TaskViewModel()
        {
            id = Guid.NewGuid();
            projectId = Guid.NewGuid();
            name = string.Empty;
            description = string.Empty;
            startDate = new DateTime();
            endDate = new DateTime();
            state = TaskStateViewModel.Draft;
            taskReports = new List<TaskReportViewModel>();
            mappingTaskUsers = new List<MappingTaskUserViewModel>();
        }

        public TaskViewModel(Guid id, Guid projectId)
        {
            this.id = id;
            this.projectId = projectId;
            this.name = string.Empty;
            description = string.Empty;
            startDate = new DateTime();
            endDate = new DateTime();
            state = TaskStateViewModel.Draft;
            taskReports = new List<TaskReportViewModel>();
            mappingTaskUsers = new List<MappingTaskUserViewModel>();
        }
        public TaskViewModel(Guid id, Guid projectId, string name, string description, DateTime startDate, DateTime endDate, TaskStateViewModel state, List<TaskReportViewModel> taskReports, List<MappingTaskUserViewModel> mappingTaskUsers)
        {
            this.id = id;
            this.projectId = projectId;
            this.name = name;
            this.description = description;
            this.startDate = startDate;
            this.endDate = endDate;
            this.state = state;
            this.taskReports = taskReports;
            this.mappingTaskUsers = mappingTaskUsers;
        }
        #endregion
    }
}