﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeReportingSystem.Models
{
    public class ProjectViewModel
    {
        Guid id;
        string name;
        string description;
        DateTime startDate;
        DateTime endDate;
        List<TaskViewModel> tasks;
        List<AccessViewModel> access;
        Guid idAdmin;


        #region Properties
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public List<TaskViewModel> Tasks
        {
            get { return tasks; }
            set { tasks = value; }
        }
        public Guid IdAdmin
        {
            get { return idAdmin; }
            set { idAdmin = value; }
        }
        public List<AccessViewModel> AccessList
        {
            get { return access; }
            set { access = value; }
        }
        #endregion

        #region Constructors
        public ProjectViewModel()
        {
            id = Guid.NewGuid();
            name = string.Empty;
            description = string.Empty;
            startDate = DateTime.Now;
            endDate = new DateTime();
            tasks = new List<TaskViewModel>();
            access = new List<AccessViewModel>();
            idAdmin = Guid.NewGuid();
        }

        public ProjectViewModel(Guid id, Guid idAdmin)
        {
            this.id = id;
            name = string.Empty;
            description = string.Empty;
            startDate = new DateTime();
            endDate = new DateTime();
            tasks = new List<TaskViewModel>();
            access = new List<AccessViewModel>();
            this.idAdmin = idAdmin;
        }
        public ProjectViewModel(Guid id, string name, string description, DateTime startDate, DateTime endDate, List<TaskViewModel> tasks, List<AccessViewModel> access, Guid idAdmin)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.startDate = startDate;
            this.endDate = endDate;
            this.tasks = tasks;
            this.access = access;
            this.idAdmin = idAdmin;
        }
        #endregion
    }
}