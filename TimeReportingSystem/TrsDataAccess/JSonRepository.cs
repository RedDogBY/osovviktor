﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrsDataAccess.Models;

namespace TrsDataAccess
{
    public class JsonRepository : IRepository<Project>
    {
        readonly string fileName;

        public JsonRepository(string fileName)
        {
            this.fileName = fileName;
        }
        public IEnumerable<Project> GetAll()
        {
            return ReaderJson<Project>.ReadFile(fileName);
        }

        public Project GetById(Guid id)
        {
            return GetAll().First(x => x.Id.Equals(id));
        }

        public void Add(Project project)
        {
            List<Project> list = GetAll().ToList();
            list.Add(project);
            WriterJson<Project>.WriteFile(list, fileName);
        }
        public void Remove(Project project)
        {
            List<Project> list = GetAll().ToList();
            list.Remove(list.Find(x => x.Id.Equals(project.Id)));
            WriterJson<Project>.WriteFile(list, fileName);
        }
        public void Modify(Project project)
        {
            IEnumerable<Project> list = GetAll();
            Project tmp = list.First(x => x.Id.Equals(project.Id));
            tmp.Name = project.Name;
            tmp.Description = project.Description;
            tmp.StartDate = project.StartDate;
            tmp.EndDate = project.EndDate;
            tmp.Tasks = project.Tasks;
            tmp.AccessList = project.AccessList;
            WriterJson<Project>.WriteFile(list, fileName);
        }


    }
}
