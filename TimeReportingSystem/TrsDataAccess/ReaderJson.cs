﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TrsDataAccess
{
    class ReaderJson<T>
    {
        public static IEnumerable<T> ReadFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            List<T> list = new List<T>();
            string tmp = string.Empty;
            while ((tmp = sr.ReadLine()) != null)
            {
                list.Add(JsonConvert.DeserializeObject<T>(tmp));
            }
            sr.Close();
            return list;
        }
    }
}
