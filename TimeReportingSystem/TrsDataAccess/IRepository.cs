﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsDataAccess
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        T GetById(Guid id);
        void Add(T item);
        void Remove(T item);
        void Modify(T item);
    }
}
