﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TrsDataAccess
{
    class WriterJson<T>
    {
        public static void WriteFile(IEnumerable<T> list, string fileName)
        {
            StreamWriter sw = new StreamWriter(fileName);
            foreach (T item in list)
            {
                sw.WriteLine(JsonConvert.SerializeObject(item));
            }
            sw.Close();
        }
    }
}
