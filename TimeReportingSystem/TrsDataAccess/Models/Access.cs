﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsDataAccess.Models
{
    public class Access
    {
        Guid id;
        Guid idProject;
        Guid idUser;
        AccessLevel level;
        string login;

        public string Login
        {
            get { return login; }
            set { login = value; }
        }
        public Guid IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }
        public Guid IdProject
        {
            get { return idProject; }
            set { idProject = value; }
        }
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public AccessLevel Level
        {
            get { return level; }
            set { level = value; }
        }

        public Access()
        {
            id = Guid.NewGuid();
            idProject = Guid.NewGuid();
            idUser = Guid.NewGuid();
            level = AccessLevel.Manager;
            this.login = string.Empty;
        }
        public Access(Guid id, Guid idProject, Guid idUser, AccessLevel accessLevel, string login)
        {
            this.Id = id;
            this.IdProject = idProject;
            this.IdUser = idUser;
            this.level = accessLevel;
            this.login = login;
        }
    }
}
