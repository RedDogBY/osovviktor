﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsDataAccess.Models
{
    public class Project
    {
        Guid id;
        string name;
        string description;
        DateTime startDate;
        DateTime endDate;
        List<Task> tasks;
        List<Access> access;
        Guid idAdmin;



        #region Properties
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public List<Task> Tasks
        {
            get { return tasks; }
            set { tasks = value; }
        }
        public List<Access> AccessList
        {
            get { return access; }
            set { access = value; }
        }
        public Guid IdAdmin
        {
            get { return idAdmin; }
            set { idAdmin = value; }
        }
        #endregion

        #region Constructors
        public Project()
        {
            id = Guid.NewGuid();
            name = string.Empty;
            description = string.Empty;
            startDate = DateTime.Now;
            endDate = new DateTime();
            tasks = new List<Task>();
            access = new List<Access>();
            idAdmin = Guid.NewGuid();
        }

        public Project(Guid id, Guid idAdmin)
        {
            this.id = id;
            name = string.Empty;
            description = string.Empty;
            startDate = new DateTime();
            endDate = new DateTime();
            tasks = new List<Task>();
            access = new List<Access>();
            this.idAdmin = idAdmin;
        }
        public Project(Guid id, string name, string description, DateTime startDate, DateTime endDate, List<Task> tasks, List<Access> mappingProjectsUsers, Guid idAdmin)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.startDate = startDate;
            this.endDate = endDate;
            this.tasks = tasks;
            this.access = access;
            this.idAdmin = idAdmin;
        }
        #endregion

        public override string ToString()
        {
            return String.Format("{0};{1};{2};{3};{4};{5}", id, name, description, startDate, endDate, tasks);
        }
    }
}
