﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrsDataAccess.Models
{
    public class Task
    {
        Guid id;
        string name;
        string description;
        DateTime startDate;
        DateTime endDate;
        TaskState state;
        List<TaskReport> taskReports;
        Guid projectId;
        List<MappingTaskUser> mappingTasksUsers;

        #region Properties
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public TaskState State
        {
            get { return state; }
            set { state = value; }
        }
        public List<TaskReport> TaskReports
        {
            get { return taskReports; }
            set { taskReports = value; }
        }
        public Guid ProjectId
        {
            get { return projectId; }
            set { projectId = value; }
        }
        public List<MappingTaskUser> MappingTasksUsers
        {
            get { return mappingTasksUsers; }
            set { mappingTasksUsers = value; }
        }
        #endregion

        #region Constructors
        public Task()
        {
            id = Guid.NewGuid();
            projectId = Guid.NewGuid();
            name = string.Empty;
            description = string.Empty;
            startDate = new DateTime();
            endDate = new DateTime();
            state = TaskState.Draft;
            taskReports = new List<TaskReport>();
            mappingTasksUsers = new List<MappingTaskUser>();
        }

        public Task(Guid id, Guid projectId)
        {
            this.id = id;
            this.projectId = projectId;
            this.name = string.Empty;
            description = string.Empty;
            startDate = new DateTime();
            endDate = new DateTime();
            state = TaskState.Draft;
            taskReports = new List<TaskReport>();
            mappingTasksUsers = new List<MappingTaskUser>();
        }
        public Task(Guid id, Guid projectId, string name, string description, DateTime startDate, DateTime endDate, TaskState state, List<TaskReport> taskReports, List<MappingTaskUser> mappingTasksUsers)
        {
            this.id = id;
            this.projectId = projectId;
            this.name = name;
            this.description = description;
            this.startDate = startDate;
            this.endDate = endDate;
            this.state = state;
            this.taskReports = taskReports;
            this.mappingTasksUsers = mappingTasksUsers;
        }
        #endregion

        public override string ToString()
        {
            return String.Format("{0};{1};{2};{3};{4};{5};{6}", id, projectId, name, description, startDate, endDate, state); ;
        }
    }
}
