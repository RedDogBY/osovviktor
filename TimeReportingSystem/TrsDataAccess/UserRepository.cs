﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrsDataAccess.Models;

namespace TrsDataAccess
{
    public class UserRepository : IRepository<User>
    {
        readonly string fileName;
        public UserRepository(string fileName)
        {
            this.fileName = fileName;
        }
        public IEnumerable<User> GetAll()
        {
            return ReaderJson<User>.ReadFile(fileName);
        }
        public User GetById(Guid id)
        {
            return GetAll().First(x => x.Id.Equals(id));
        }
        public void Add(User user)
        {
            List<User> list = GetAll().ToList();
            list.Add(user);
            WriterJson<User>.WriteFile(list, fileName);
        }
        public void Remove(User user)
        {
            List<User> list = GetAll().ToList();
            list.Remove(list.Find(x => x.Id.Equals(user.Id)));
            WriterJson<User>.WriteFile(list, fileName);
        }
        public void Modify(User user)
        {
            IEnumerable<User> list = GetAll();
            User tmp = list.First(x => x.Id.Equals(user.Id));
            tmp.FirstName = user.FirstName;
            tmp.LastName = user.LastName;
            tmp.Login = user.Login;
            tmp.Password = user.Password;
            tmp.Locale = user.Locale;
            tmp.TimeZone = user.TimeZone;
            WriterJson<User>.WriteFile(list, fileName);
        }
    }
}
